<template>
   <v-layout class="chartContainer">
      <v-flex>
         <v-card v-for="year in years" class="chartCard">
            <v-card-title>
               <h1 class="headline">{{year}}</h1>
            </v-card-title>
            <v-card-text>
               <BarChart class="atChart" :options="options" :chartData="getDatasetForYear(year)"/>
            </v-card-text>
         </v-card>
         <v-card class="chartCard">
            <v-card-title>
               <h1 class="headline">Debet</h1>
            </v-card-title>
            <v-card-text>
               <BarChart class="atChart" :options="options" :chartData="getDatasetDebet()"/>
            </v-card-text>
         </v-card>
         <v-card class="chartCard">
            <v-card-title>
               <h1 class="headline">Credit</h1>
            </v-card-title>
            <v-card-text>
               <BarChart class="atChart" :options="options" :chartData="getDatasetCredit()"/>
            </v-card-text>
         </v-card>
      </v-flex>
   </v-layout>
</template>

<script>
	import { isDebet, isCredit, sum, getYears } from '@/js/lib/TransactionUtil';
	import { MONTHS } from '@/js/lib/DateUtil';
	import { flatten } from '@/js/lib/ArrayUtil';
	import BarChart from './BarChart.js';
	import { mapState } from 'vuex';

	function toDataset(data) {
		return {
			labels: MONTHS,
			datasets: data
		};
	}

	function getTransactions(ts, year, matchFnc) {
		const tsFiltered = ts.filter(t => t.date.isFromYear(year)).filter(matchFnc);
		return MONTHS.map((month, idx) => {
			return Math.abs(Math.round(sum(tsFiltered.filter(t => t.date.isFromMonth(idx + 1)).map(t => t.amount))));
		});
	}

	export default {
		components: {
			BarChart,
		},
		computed: {
			...mapState(['transactions']),
			years() {
				return getYears(this.transactions).reverse();
			},
		},
		data() {
			return {
				options: {
					maintainAspectRatio: false,
				}
			}
		},
		methods: {
			getDatasetCredit() {
				const ts = this.years.map(year => {
					return {
						label: `Credit ${year}`,
						backgroundColor: '#22aa33',
						data: getTransactions(this.transactions, year, isCredit),
					};
				});
				return toDataset(ts);
			},
			getDatasetDebet() {
				const ts = this.years.map(year => {
					return {
						label: `Debet ${year}`,
						backgroundColor: '#941c24',
						data: getTransactions(this.transactions, year, isDebet),
					};
				});
				return toDataset(ts);
			},
			getDatasetForYear(year) {
				return toDataset([{
					label: 'Debet',
					backgroundColor: '#941c24',
					data: getTransactions(this.transactions, year, isDebet),
				}, {
					label: 'Credit',
					backgroundColor: '#22aa33',
					data: getTransactions(this.transactions, year, isCredit),
				}]);
			}
		}
	}
</script>
<style scoped>
   .chartCard {
      margin: 20px;
      /*background-color: #292929 !important;*/
   }

   .atChart {
      background-color: #333 !important;
   }
</style>
