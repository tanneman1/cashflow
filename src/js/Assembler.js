import injector from 'vue-inject';
import Bootstrapper from './Bootstrapper'
import ServerApi from './ServerApi'
import RetrieveTransactionsAction from './actions/RetrieveTransactionsAction';
import RetrieveCategoriesAction from './actions/RetrieveCategoriesAction';
import FilterAction from './actions/FilterAction';

export function assemble(store, router) {
	injector.constant('store', store);
	injector.constant('router', router);
	injector.service('serverApi', ServerApi);
	injector.service('retrieveTransactionsAction', ['serverApi', 'store'], RetrieveTransactionsAction);
	injector.service('retrieveCategoriesAction', ['serverApi', 'store'], RetrieveCategoriesAction);
	injector.service('filterAction', ['store'], FilterAction);
	injector.service('bootstrapper', ['retrieveTransactionsAction', 'retrieveCategoriesAction'], Bootstrapper);
}