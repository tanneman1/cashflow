import { removeDuplicates } from '@/js/lib/ArrayUtil';

export function isDebet(t) {
	return Number(t.amount) < 0;
}

export function isCredit(t) {
	return !isDebet(t);
}

export function sum(arr) {
	return arr.reduce((sum, v) => sum + v, 0);
}

export function getYears(ts) {
	const years = ts.map(t => Number(t.date.year));
	const filteredYears = removeDuplicates(years);
	filteredYears.sort();
	return filteredYears;
}

export function getAccounts(ts) {
	const accounts = ts.map(t => t.account);
	const filteredAccounts = removeDuplicates(accounts, (a, b) => {
		return a.number === b.number
	});
	filteredAccounts.sort();
	return filteredAccounts;
}

export function getDates(ts) {
	const allDates = ts.map(t => t.date);
	const dates = removeDuplicates(allDates, (a, b) => {
		return a.year === b.year && a.month === b.month;
	});
	dates.sort((a, b) => {
		return a._date.getTime() - b._date.getTime();
	});
	return dates;
}

export function mapToDate(ts, dates) {
	return dates.map((date) => {
		return Math.abs(Math.round(sum(ts.filter(t => t.date.isFromYear(date.year) && t.date.isFromMonth(date.month)).map(t => t.amount)
		)));
	});
}
