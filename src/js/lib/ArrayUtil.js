export function flatten(arr) {
	return [].concat(...arr);
}

export function removeDuplicates(arr, matchFunction) {
	if (matchFunction) {
		return removeDuplicatesWithFunction(arr, matchFunction);
	}
	return arr.reduce((uniqueValues, item) => {
		if (uniqueValues.indexOf(item) === -1) {
			uniqueValues.push(item);
		}
		return uniqueValues;
	}, []);
}

export function removeDuplicatesWithFunction(arr, matchFunction) {
	return arr.reduce((uniqueValues, item) => {
		if (!uniqueValues.find(uniqueItem => matchFunction(uniqueItem, item))) {
			uniqueValues.push(item);
		}
		return uniqueValues;
	}, []);
}
