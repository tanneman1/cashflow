import { flatten } from '@/js/lib/ArrayUtil';

export function filterAccount(accounts) {
	if (accounts.length === 0) {
		return t => true;
	}
	return t => accounts.includes(t.account.number);
}

export function filterCategories(categories) {
	if (categories.length === 0) {
		return t => true;
	}
	return t => categories.includes(t.categorie);
}

export function filterYears(years) {
	if (years.length === 0) {
		return t => true;
	}
	return t => years.includes(Number(t.date.year));
}

export function filterYearAndMonth(dates) {
	if (dates.length === 0) {
		return t => true;
	}
	return t => {
		return dates.some(d => t.date.isFromYearAndMonth(d));
	}
}

export function filterText(text) {
	if (!text) {
		return t => true;
	}
	return t => {
		return [t.description, t.date.display, t.amount.toString(), t.categorie]
			.some(attr => attr.toLowerCase().includes(text.toLowerCase()));
	};
}