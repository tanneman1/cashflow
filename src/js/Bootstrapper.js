class Bootstrapper {
	constructor(retrieveTransactionsAction, retrieveCategoriesAction) {
		this.retrieveTransactionsAction = retrieveTransactionsAction;
		this.retrieveCategoriesAction = retrieveCategoriesAction;
	}

	async bootstrap() {
		await this.retrieveCategoriesAction.execute();
		await this.retrieveTransactionsAction.execute();
	}
}

export default Bootstrapper;
