import axios from 'axios';

export default class ServerApi {

	async getTransactions() {
		const data = await axios.get('/cashflow/data.php?data=rbc');
		return data;
	}

	async getCategories() {
		const data = await axios.get('categories.json');
		return data;
	}
}