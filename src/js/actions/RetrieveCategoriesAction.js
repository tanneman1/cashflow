import axios from 'axios';

export default class RetrieveCategoriesAction {

	constructor(serverApi, store) {
		this.serverApi = serverApi;
		this.store = store;
	}

	async execute() {
		const resp = await this.serverApi.getCategories();
		this.store.commit('categories', resp.data);
	}
}