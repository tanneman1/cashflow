import axios from 'axios';
import { parseTransactions } from '@/js/TransactionParser';

export default class RetrieveTransactionsAction {

	constructor(serverApi, store) {
		this.serverApi = serverApi;
		this.store = store;
	}

	async execute() {
		this.store.commit('loading', true);
		const resp = await this.serverApi.getTransactions();
		const transactions = parseTransactions(resp.data, this.store.state.categories);
		this.store.commit('allTransactions', transactions);
		this.store.commit('transactions', transactions);
		this.store.commit('loading', false);
	}
}