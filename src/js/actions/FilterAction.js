import { filterAccount, filterCategories, filterYears, filterText } from '@/js/lib/FilterUtil';
import { flatten } from '@/js/lib/ArrayUtil';

export default class FilterAction {

	constructor(store) {
		this.store = store;
	}

	async execute(filters) {
		this.store.commit('loading', true);
		const filteredTransactions = this.store.state.allTransactions
			.filter(filterYears(filters.years))
			.filter(filterCategories(filters.categories))
			.filter(filterText(filters.filterText))
			.filter(filterAccount(filters.accounts));
		this.store.commit('transactions', filteredTransactions);
		this.store.commit('loading', false);
	}
}