import { removeDuplicates } from '@/js/lib/ArrayUtil.js';

class Account {
	constructor(accountType, accountNumber) {
		this.accountTypeValue = accountType;
		this.accountNumberValue = accountNumber;
	}

	get type() {
		return this.accountTypeValue;
	}

	get number() {
		return this.accountNumberValue;
	}

	toString() {
		return `${this.accountTypeValue}|${this.accountNumberValue}`;
	}
}

class Transaction {
	constructor(id, account, date, description, amount, categorie, raw) {
		this.idValue = id;
		this.accountValue = account;
		this.dateValue = date;
		this.descriptionValue = description;
		this.amountValue = amount;
		this.categorieValue = categorie;
		this.rawValue = raw;
	}

	get raw() {
		return this.rawValue;
	}

	get categorie() {
		return this.categorieValue;
	}

	get id() {
		return this.idValue;
	}

	get account() {
		return this.accountValue;
	}

	get date() {
		return this.dateValue;
	}

	get description() {
		return this.descriptionValue;
	}

	get amount() {
		return this.amountValue;
	}

	toString() {
		return `${this.account}|${this.date}|${this.description}|${this.amount}`;
	}
}

class TDate {
	constructor(year, month, day) {
		this.yearValue = year;
		this.monthValue = month;
		this.dayValue = day;
		this._date = new Date(year, month - 1, day);

		const monthStr = this.monthValue.toString().padStart(2, '0');
		const dayStr = this.dayValue.toString().padStart(2, '0');
		this.displayValue = `${year}-${monthStr}-${dayStr}`;

	}

	isBeforeDate(date) {
		return this._date < date._date;
	}

	isFromYearAndMonth(date) {
		return this.isFromYear(date.year) && this.isFromMonth(date.month);
	}

	isFromYear(year) {
		return year === Number(this.yearValue);
	}

	isFromMonth(month) {
		return month === Number(this.monthValue);
	}

	get year() {
		return this.yearValue;
	}

	get month() {
		return this.monthValue;
	}

	get day() {
		return this.dayValue;
	}

	get display() {
		return this.displayValue;
	}
}

function formatDate(dateStr) {
	const regex = new RegExp('(.*)\\/(.*)\\/(\\d{4})');

	const output = regex.exec(dateStr);
	const year = Number(output[3]);
	const month = Number(output[1]);
	const day = Number(output[2]);
	return new TDate(year, month, day);
}

function determineCategories(categoryMap, description) {
	const categories = Object.keys(categoryMap);
	const cat = categories.find(c => {
		return categoryMap[c].find(tc => description.toLowerCase().includes(tc.toLowerCase()));
	});
	return cat || 'Unknown';
}

function createTransaction(categoryMap) {
	return (t) => {
		const id = `${t[1]}_${t[2]}_${t[6]}`;
		const date = formatDate(t[2]);
		const description = `${t[4]} ${t[5]}`;
		const categorie = determineCategories(categoryMap, description);
		return new Transaction(id, new Account(t[0], t[1]), date, description, Number(t[6]), categorie, t);
	}
}

export function parseTransactions(transactions, categoryMap) {
	const ts = transactions.slice(1).map(createTransaction(categoryMap));
	return removeDuplicates(ts, (a, b) => a.id === b.id);
}