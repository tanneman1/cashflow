import '@babel/polyfill'
import Vue from 'vue'
import Vuetify from 'vuetify';
import injector from 'vue-inject';
import './plugins/vuetify'
import App from './App.vue'

import store from './store';
import router from './router';

import { assemble } from './js/Assembler'

import 'vuetify/dist/vuetify.min.css';

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(injector);

assemble(store, router);

new Vue({
	store,
	router,
	render: h => h(App),
}).$mount('#app');

injector.get('bootstrapper').bootstrap();