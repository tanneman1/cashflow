import Vue from 'vue';
import Router from 'vue-router';
import TransactionsView from '../components/TransactionsView.vue';
import ChartView from '../components/ChartView.vue';
import CumulativeView from '../components/CumulativeView.vue';
import TableView from '../components/TableView.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'Main',
			component: TransactionsView,
		},
		{
			path: '/transactions',
			name: 'TransactionsView',
			component: TransactionsView,
		},
		{
			path: '/cumulative',
			name: 'CumulativeView',
			component: CumulativeView,
		},
		{
			path: '/table',
			name: 'TableView',
			component: TableView,
		},
		{
			path: '/chart',
			name: 'ChartView',
			component: ChartView,
		},
	],
});

