import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {},
	state: {
		errors: [],
		loading: false,
		transactions: [],
		allTransactions: [],
		categories: {},
	},
	mutations: {
		error(state, value) {
			state.errors.push(value);
		},
		errors(state, value) {
			state.errors = state.errors.concat(value);
		},
		loading(state, value) {
			state.loading = value;
		},
		transactions(state, value) {
			state.transactions = value;
		},
		allTransactions(state, value) {
			state.allTransactions = value;
		},
		categories(state, value) {
			state.categories = value;
		},
	},
});

export default store;
