# cashflow
A cashflow app written in Vue2. It displays all your transactions and allows you to filter on description, year, account and categories (user defined). It also shows graphs for credit and debit and a table with the totals per month.

Backend (separate repo) is PHP that serves csv files as JSON and a user defined category map.

## Sample screen
Screenshot of the credit/debit totals per month.
![Screenshot 1](/graph.png?raw=true "Displaying the graph")

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
